//
//  AppDelegate.swift
//  ATCrypto
//
//  Created by Dejan on 04/03/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit
import CryptoSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let testString = "Well, the way they make shows is, they make one show. That show's called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows. Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing."
        
        let sha = testString.sha512()
        print("sha: \(sha)")
        
        let crc = testString.crc32()
        print("crc: \(crc)")
        
        let md5 = testString.md5()
        print("md5: \(md5)")
        
        if let base64cipher = try? Rabbit(key: "1234567890123456"),
            let base64 = try? testString.encryptToBase64(cipher: base64cipher) {
            print("encrypted base64: \(base64!)")
            
            let decrypted = try? base64!.decryptBase64ToString(cipher: base64cipher)
            print("decrypted base64: \(decrypted!)")
        }
        
        if let aes = try? AES(key: "1234567890123456", iv: "abdefdsrfjdirogf"),
            let aesE = try? aes.encrypt(Array(testString.utf8)) {
            print("AES encrypted: \(aesE)")
            
            let aesD = try? aes.decrypt(aesE)
            let decrypted = String(bytes: aesD!, encoding: .utf8)
            print("AES decrypted: \(decrypted)")
        }
        
        return true
    }
}

